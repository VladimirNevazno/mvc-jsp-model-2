<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.belhard.v5Class.v5ClassServlet"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>zadanie_5_MVC.jsp</title>
<link rel="stylesheet" type="text/css" href="css/mysite.css">
</head>
<body>
	<jsp:useBean id="memory5" type="com.belhard.v5Class.MemoryBean"
		scope="session" />
	<table border="1" width="100%" height="260" cellpadding="5">
		<tr>
			<th>

				<form action="class_1_5.class" method="post">

					<p>
						Длина &nbsp; <input type="number"
							name="paramDlina"
							value="<jsp:getProperty property="dlina" name="memory5" />">
						
					</p>
					<br>
					<p>
						Ширина &nbsp; <input type="number" name="paramShirina"
							value="<jsp:getProperty property="shirina" name="memory5" />">
					</p>
					<br> <input type="submit" value="Вычислить">

				</form>
<form action="class_1_5.class" method="post">
	<jsp:setProperty property="dlina" name="memory5" value="" />
	<jsp:setProperty property="shirina" name="memory5" value="" />
	<input type="submit" value="reset">
</form>
<h2>Периметр:&nbsp;<jsp:getProperty property="perimetr" name="memory5" /></h2>
<h2>Площадь:&nbsp;<jsp:getProperty property="ploshad" name="memory5" /></h2>
<h2>Длина диагонали:&nbsp;<jsp:getProperty property="dlinaDiagonali" name="memory5" /></h2>
			</th>
		</tr>
	</table>
	<table border="1" width="100%" height="70" cellpadding="5">
		<tr>
			<th width='100%'>
  				<h1><a href="index.html">Go To Index Page</a></h1>
   			</th>
		</tr>
	</table>
</body>
</html>