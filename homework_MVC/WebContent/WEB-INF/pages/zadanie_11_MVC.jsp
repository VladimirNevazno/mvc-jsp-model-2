<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>zadanie_11_MVC.jsp</title>
<link rel="stylesheet" type="text/css" href="css/mysite.css">
</head>
<body>
<jsp:useBean id="memory11" type="com.belhard.v11Class.MemoryBean" scope="session"></jsp:useBean>
	<table border="1" width="100%" height="350" cellpadding="5">
		<tr>
		<th>
<h1><jsp:getProperty property="vopros" name="memory11"/></h1><br>

<form action="class_1_11.class" method="post">
<h2><jsp:getProperty property="variant1" name="memory11"/>
	<jsp:getProperty property="variant2" name="memory11"/>
	<jsp:getProperty property="variant3" name="memory11"/>
	<jsp:getProperty property="variant4" name="memory11"/></h2>
</form>

<h2><jsp:getProperty property="oldVopros" name="memory11"/>
	<br><jsp:getProperty property="oldOtvet" name="memory11"/>
	<br><jsp:getProperty property="otvet" name="memory11"/>
	<br><jsp:getProperty property="button" name="memory11"/></h2>
		</th>
		</tr>
	</table>
	<table border="1" width="100%" height="70" cellpadding="5">
		<tr>
			<th width='100%'>
  				<h1><a href="index.html">Go To Index Page</a></h1>
   			</th>
		</tr>
	</table>
</body>
</html>