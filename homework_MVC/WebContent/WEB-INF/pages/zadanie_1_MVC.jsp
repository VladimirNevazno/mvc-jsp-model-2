<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>zadanie_1_MVC</title>
<link rel="stylesheet" type="text/css" href="css/mysite.css">
</head>
<body>
<jsp:useBean id="memory1" type="com.belhard.v1Class.MemoryBean" scope="session" />
   
	<table border="1" width="100%" height="480" cellpadding="5">
		<tr>
			<th width='100%'>
				<h1>На какой картинке пингвинчики?</h1><br/>
				<a href='class_1_1.class?param=1'><img src='pictureTest/1.jpg' height='180' /></a>
				<a href='class_1_1.class?param=2'><img src='pictureTest/2.jpg' height='180' /></a>
				<a href='class_1_1.class?param=3'><img src='pictureTest/3.jpg' height='180' /></a>
				<a href='class_1_1.class?param=4'><img src='pictureTest/4.jpg' height='180' /></a>

				<jsp:getProperty property="stroka1" name="memory1"/>
				<jsp:getProperty property="stroka2" name="memory1"/>
				<jsp:getProperty property="stroka3" name="memory1"/>

			</th>
		</tr>
	</table>
   
	<table border="1" width="100%" height="70" cellpadding="5">
		<tr>
			<th width='100%'>
  				<h1><a href="index.html">Go To Index Page</a></h1>
   			</th>
		</tr>
	</table>
</body>
</html>