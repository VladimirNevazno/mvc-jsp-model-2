<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>zadanie_4_MVC.jsp</title>
<link rel="stylesheet" type="text/css" href="css/mysite.css">
</head>
<body>
	<jsp:useBean id="memory4" type="com.belhard.v4Class.MemoryBean" scope="session"></jsp:useBean>
	<table border="1" width="100%" height="260" cellpadding="5">
		<tr>
			<th>
				<form action="class_1_4.class" method="post">
					<p><jsp:getProperty property="clarification" name="memory4" />
					<br><jsp:getProperty property="otvet" name="memory4" />
					<br>Напечатайте число&nbsp;
						<jsp:getProperty property="parTextNumber" name="memory4" />
						
						<br><input type="text" name="paramText">
					<br><input type="submit" value="Отправить"></p>
				</form>

			</th>
		</tr>
	</table>
	<table border="1" width="100%" height="70" cellpadding="5">
		<tr>
			<th width='100%'>
  				<h1><a href="index.html">Go To Index Page</a></h1>
   			</th>
		</tr>
	</table>
</body>
</html>