<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>второе задание по MVC</title>
<link rel="stylesheet" type="text/css" href="css/mysite.css">
</head>
<body>

<jsp:useBean id="memory2" type="com.belhard.v2Class.MemoryBean" scope="session" />

	<table border="1" width="100%" height="50" cellpadding="5">
		<tr>
    		<th><h2>Количество правильных ответов 
    		<jsp:getProperty property="x" name="memory2"/>% (
    		<jsp:getProperty property="n" name="memory2"/> ответов из 
    		<jsp:getProperty property="m" name="memory2"/>)
    		</h2></th>
  		</tr>
	</table>
	
	<table border="1" width="100%" height="50" cellpadding="5">
		<tr>
<th>
<form action="class_1_2.class" method="get">
<h2><button type="submit" name="param1" value="1">следующее задание</button></h2>
</form>
</th>
  		</tr>
	</table>
	
	<table border="1" width="100%" height="260" cellpadding="5">
		<tr>
			<th>
    		<form action="class_1_2.class" method="get">
    		
		  	<p><b>
		  		<jsp:getProperty property="primer" name="memory2"/>
		  	</b></p>
				  <p>
<input type="radio" name="answer" value="<jsp:getProperty property="otvet1" name="memory2"/>">
						<jsp:getProperty property="otvet1" name="memory2"/><Br>
<input type="radio" name="answer" value="<jsp:getProperty property="otvet2" name="memory2"/>">
				 		<jsp:getProperty property="otvet2" name="memory2"/><Br>
<input type="radio" name="answer" value="<jsp:getProperty property="otvet3" name="memory2"/>">
				  		<jsp:getProperty property="otvet3" name="memory2"/><Br>
<input type="radio" name="answer" value="<jsp:getProperty property="otvet4" name="memory2"/>">
				  		<jsp:getProperty property="otvet4" name="memory2"/>
				  </p>
				  <p><button type="submit" name="param1" value="1">ответить</button></p>
 			</form>
 			</th>
  		</tr>
	</table>
	
	<table border="1" width="100%" height="50" cellpadding="5">
		<tr>
    		<th>
    			<h1><jsp:getProperty property="checkTheAnswer" name="memory2"/></h1>
    			<br/><h1><a href="index.html">Go To Index Page</a></h1>
    		</th>
  		</tr>
	</table>
	
</body>
</html>