﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>zadanie_8_MVC.jsp</title>
	<link rel="stylesheet" type="text/css" href="css/mysite.css">
</head>

<body>
	<form action="class_1_8.class" method="post" name="transliteration" >
		<jsp:useBean id="translitBean" type="com.belhard.v8Class.TranslitBean" scope="session"></jsp:useBean>
	
		<label for="enter">Russian text:</label><br>
		<textarea id="enter" name="enter" rows="10" cols="45">
			<jsp:getProperty property="russianText" name="translitBean"/>
		</textarea>
		<br>

		<label for="exit">Transliteration:</label><br>
		<textarea id="exit" name="exit" rows="10" cols="45">
			<jsp:getProperty property="translitText" name="translitBean"/>
		</textarea>
		<br>

		<input type="submit" name="translit" value="Translit">	
	</form>
	<table border="1" width="100%" height="70" cellpadding="5">
		<tr>
			<th width='100%'>
  				<h1><a href="index.html">Go To Index Page</a></h1>
   			</th>
		</tr>
	</table>
</body>

</html>