package com.belhard.v9Class;

import java.io.IOException;
import java.util.Arrays;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class v9ClassServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		// приём из сессии
		MemoryBean memory9 = (MemoryBean) session.getAttribute("memory9");
		if (memory9 == null)
			memory9 = new MemoryBean();

		// приём параметров
		String inputPicture = request.getParameter("paramPicture");
		
		String inputParam = request.getParameter("param");
		if (inputParam==null) inputParam="0";
		int parameter = Integer.parseInt(inputParam);

		// поступление списка картинок
		if (inputPicture != null) {
			// записываем массив названий файлов, получаем длинну массива, индекс
			// делаем 0, отображаем первую картинку. создаём код вывода картинок в "pictures"
			String[] tmp = inputPicture.split(":");
			memory9.setNamesOfictures(tmp);//1
			memory9.setLengthOfTheArray(Arrays.asList(tmp).size());//2
			memory9.setIndexPicture(0);//3
			tmp = memory9.getNamesOfictures();
			memory9.setPictureName(tmp[0]);//4
		//pictures
			memory9.setPictures("");//5
			for (int i=1;i<=memory9.getLengthOfTheArray();i++) {
			memory9.setPictures(memory9.getPictures()+"<a href='class_1_9.class?param="+i+"'><img src='pictureTest/"+i+".jpg' height='60' /></a>");
			}
		}
		if (parameter!=0) {
			String[] tmp = memory9.getNamesOfictures();
			memory9.setPictureName(tmp[parameter-1]);
		}
		// запаковали сессию
		session.setAttribute("memory9", memory9);

		// формирование ответа
		RequestDispatcher dispatcher = request
				.getRequestDispatcher("/WEB-INF/pages/zadanie_9_MVC.jsp");
		dispatcher.forward(request, response);
	}
}
