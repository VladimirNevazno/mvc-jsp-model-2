package com.belhard.v9Class;

public class MemoryBean {

	private String pictureName = "";// 4
	private int indexPicture = 0;// 3
	private String[] namesOfictures = null;// 1
	private int lengthOfTheArray = 0;// 2
	private String pictures = "";//5

	public MemoryBean() {
		pictureName = "";
		indexPicture = 0;
		namesOfictures = null;
		lengthOfTheArray = 0;
	}

	public String getPictureName() {
		return pictureName;
	}

	public void setPictureName(String pictureName) {
		this.pictureName = pictureName;
	}

	public int getIndexPicture() {
		return indexPicture;
	}

	public void setIndexPicture(int indexPicture) {
		this.indexPicture = indexPicture;
	}

	public String[] getNamesOfictures() {
		return namesOfictures;
	}

	public void setNamesOfictures(String[] namesOfictures) {
		this.namesOfictures = namesOfictures;
	}

	public int getLengthOfTheArray() {
		return lengthOfTheArray;
	}

	public void setLengthOfTheArray(int lengthOfTheArray) {
		this.lengthOfTheArray = lengthOfTheArray;
	}

	public String getPictures() {
		return pictures;
	}

	public void setPictures(String pictures) {
		this.pictures = pictures;
	}

}