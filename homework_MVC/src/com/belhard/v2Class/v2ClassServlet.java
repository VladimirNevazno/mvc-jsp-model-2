package com.belhard.v2Class;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class v2ClassServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		// приём из сессии
				MemoryBean memory2 = (MemoryBean) session.getAttribute("memory2");
				if (memory2 == null) memory2 = new MemoryBean();
		request.setCharacterEncoding("UTF-8");
		
		//Приём параметров
		String incomingAnswer = request.getParameter("answer");
		String inputParam1 = request.getParameter("param1");
			
		//Тут бизнес-код
		//Проверка ответа
		if (incomingAnswer!=null) {
			memory2.setM(memory2.getM()+1);
			if (memory2.getCorrectAnswer().equals(incomingAnswer)) {
				memory2.setN(memory2.getN()+1);
				memory2.setCheckTheAnswer("Правильно! Ответ "+memory2.getCorrectAnswer());
			}
			else {
				memory2.setCheckTheAnswer("Ошибка! Правильный ответ "+memory2.getCorrectAnswer());
			}
			memory2.setX(100*(memory2.getN()/memory2.getM()));
		}
		
		//Генерация чисел
	if (inputParam1!=null && inputParam1.equals("1")) {
		 Random generator = new Random();
		int r1 = generator.nextInt(10)+1;
		int r2 = generator.nextInt(10)+1;
		if (r1==r2) {
	        try {
	            Thread.currentThread().sleep(5);
	            r2=generator.nextInt(10)+1;
	        }
	        catch (InterruptedException ex) {}
		}
		int r3 = generator.nextInt(10)+1;//получаем рандом число 0..9
		if (r2==r3) {
	        try {
	            Thread.currentThread().sleep(7);
	            r3=generator.nextInt(10)+1;
	        }
	        catch (InterruptedException ex) {}
		}   
		int r4 = generator.nextInt(10)+1;//получаем рандом число 0..9
		if (r3==r4) {
	        try {
	            Thread.currentThread().sleep(16);
	            r4=generator.nextInt(10)+1;
	        }
	        catch (InterruptedException ex) {}
		}
		//формирование строк
		memory2.setPrimer(r1+" X "+r4+" =");
		memory2.setCorrectAnswer(""+(r1*r4));
		//сделать перетасовку строк
		ArrayList<Integer> list = new ArrayList<Integer>();
		list.add(r1*r4); list.add(r1*r3); list.add(r4*r2); list.add(r2*r3);
		
		Collections.shuffle(list);
		memory2.setOtvet1(""+list.get(0));
		memory2.setOtvet2(""+list.get(1));
		memory2.setOtvet3(""+list.get(2));
		memory2.setOtvet4(""+list.get(3));
	}
	
		//запаковали сессию
		session.setAttribute("memory2", memory2);
		
		//формирование ответа
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/pages/zadanie_2_MVC.jsp");
		dispatcher.forward(request, response);
	}

}
