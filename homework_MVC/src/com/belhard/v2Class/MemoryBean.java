package com.belhard.v2Class;

public class MemoryBean {

	private String checkTheAnswer = "";
	private String answer = "";
	private String primer = "";
	private String otvet1 = "";
	private String otvet2 = "";
	private String otvet3 = "";
	private String otvet4 = "";
	private double x = 0;//%правильных ответов
	private double n = 0;//правильных ответов
	private double m = 0;//всего ответов
	private String correctAnswer = "";
	
	public String getCheckTheAnswer() {
		return checkTheAnswer;
	}
	public void setCheckTheAnswer(String checkTheAnswer) {
		this.checkTheAnswer = checkTheAnswer;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getPrimer() {
		return primer;
	}
	public void setPrimer(String primer) {
		this.primer = primer;
	}
	public String getOtvet1() {
		return otvet1;
	}
	public void setOtvet1(String otvet1) {
		this.otvet1 = otvet1;
	}
	public String getOtvet2() {
		return otvet2;
	}
	public void setOtvet2(String otvet2) {
		this.otvet2 = otvet2;
	}
	public String getOtvet3() {
		return otvet3;
	}
	public void setOtvet3(String otvet3) {
		this.otvet3 = otvet3;
	}
	public String getOtvet4() {
		return otvet4;
	}
	public void setOtvet4(String otvet4) {
		this.otvet4 = otvet4;
	}
	
	public double getM() {
		return m;
	}
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getN() {
		return n;
	}
	public void setN(double n) {
		this.n = n;
	}
	public void setM(double m) {
		this.m = m;
	}
	public String getCorrectAnswer() {
		return correctAnswer;
	}
	public void setCorrectAnswer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}


}