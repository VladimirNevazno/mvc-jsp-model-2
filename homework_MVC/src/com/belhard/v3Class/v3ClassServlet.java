package com.belhard.v3Class;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class v3ClassServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		MemoryBean memory3 = new MemoryBean();

		String inTextFar = request.getParameter("textFar");
		String inTextCel = request.getParameter("textCel");

		if (inTextFar != null && !inTextFar.equals("") && inTextCel != null
				&& !inTextCel.equals("")) {
			memory3.setCel("Введите значение только в одно поле");
			memory3.setFar("");
		} else {
			if (inTextFar != null && !inTextFar.equals("")) {
				double newDoubInFar = Double.parseDouble(inTextFar);
				double InCel = (((newDoubInFar - 32) * 5) / 9);
				memory3.setCel("" + InCel);
				memory3.setFar("" + newDoubInFar);
			}

			if (inTextCel != null && !inTextCel.equals("")) {
				double newDoubInCel = Double.parseDouble(inTextCel);
				double InFar = ((newDoubInCel * 9 / 5) + 32);
				memory3.setFar("" + InFar);
				memory3.setCel("" + newDoubInCel);
			}
		}
		// получили и снова запаковали сессию
		HttpSession session = request.getSession();
		session.setAttribute("memory3", memory3);

		// формирование ответа
		RequestDispatcher dispatcher = request
				.getRequestDispatcher("/WEB-INF/pages/zadanie_3_MVC.jsp");
		dispatcher.forward(request, response);
	}

}
