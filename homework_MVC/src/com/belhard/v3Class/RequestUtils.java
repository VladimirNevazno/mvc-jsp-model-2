package com.belhard.v3Class;

import javax.servlet.http.HttpServletRequest;

public final class RequestUtils {

	 private RequestUtils() {
	  super();
	 }

	 public static String getParameter(HttpServletRequest request, String paramName) {
	  if (StringUtils.isBlank(paramName)) {
	   throw new IllegalArgumentException();
	  }

	  String value = request.getParameter(paramName);
	  if (value != null) {
	   return value;
	  }

	  return StringUtils.EMPTY_STR;
	 }

	 public static Double getDoubleParameter(HttpServletRequest request, String paramName) {
		 String strValue = getParameter(request, paramName);
		 try {
			   return new Double(strValue);
			  } catch (Exception e) {
			   return null;
			  }
		 }

}