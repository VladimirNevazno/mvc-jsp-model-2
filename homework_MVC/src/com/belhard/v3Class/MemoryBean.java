package com.belhard.v3Class;

public class MemoryBean {

	private String far = "";
	private String cel = "";
	
	public String getFar() {
		return far;
	}
	public void setFar(String far) {
		this.far = far;
	}
	public String getCel() {
		return cel;
	}
	public void setCel(String cel) {
		this.cel = cel;
	}
	
}