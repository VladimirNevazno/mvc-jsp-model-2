package com.belhard.v4Class;

import java.io.IOException;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class v4ClassServlet extends HttpServlet {		
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		
		// приём из сессии
		MemoryBean memory4 = (MemoryBean) session.getAttribute("memory4");
			if (memory4 == null) memory4 = new MemoryBean();
		
		// приём параметров
		String inputText = request.getParameter("paramText");
		
		// тут бизнес-код.
		//принятый праметр сравниваем
		if (inputText!=null) {
			memory4.setOldInputText(inputText);//записали принятый ответ
			if (memory4.getParTextNumber().equals(NumberService.numberMap.get(inputText))) {
				memory4.setOtvet("Правильно");
			}
			else memory4.setOtvet("Не правильно");
		}
		//Записываем старое значение
		memory4.setOldParTextNumber(memory4.getParTextNumber());
		//Записываем пояснение к ответу
		if (!memory4.getOldParTextNumber().equals("")) 
memory4.setClarification("Вас просили напечатать число "+memory4.getOldParTextNumber()+", вы напечатали число "+memory4.getOldInputText());
		//запись прописью цифры в вопрос
		Random generator = new Random();
		String str = NumberService.numbersList[generator.nextInt(10)];//случайное число прописью
		memory4.setParTextNumber(str);
		
		// запаковали сессию
		session.setAttribute("memory4", memory4);
		
		//формирование ответа
		//response.setCharacterEncoding("UTF-8");
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/pages/zadanie_4_MVC.jsp");
		dispatcher.forward(request, response);
	}

}
