package com.belhard.v4Class;

import java.util.HashMap;
import java.util.Map;

public final class NumberService {

	public static final Map<String, String> numberMap = new HashMap<String, String>();

	static {
		numberMap.put("0", "ноль"); 
		numberMap.put("1", "один");
		numberMap.put("2", "два");
		numberMap.put("3", "три");
		numberMap.put("4", "четыре");
		numberMap.put("5", "пять");
		numberMap.put("6", "шесть"); 
		numberMap.put("7", "семь");
		numberMap.put("8", "восемь");
		numberMap.put("9", "девять");
		
	}
	
	public static final String[] numbersList =
		{"ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять"};

	private NumberService() {
		super();
	}

	public static String strToTranslit(String str) {
		char[] russianTextChars = str.toCharArray();
		StringBuilder translitTextStrings = new StringBuilder();

		for(int i = 0; i < russianTextChars.length; i++){
			char symbol = russianTextChars[i];
			String translitSymbol = NumberService.symbolToTranslit(symbol);
			translitTextStrings.append(translitSymbol);			
		}

		return translitTextStrings.toString();
	}

	public static String symbolToTranslit(char symbol) {
		String translit = numberMap.get(symbol);
		if (translit != null) {
			return translit;
		}

		return Character.toString(symbol);
	}
}
