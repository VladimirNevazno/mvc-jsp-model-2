package com.belhard.v4Class;

public class MemoryBean {

	private String otvet = "";//правильно или не правильно
	private String parTextNumber = "";//цифра прописью, которое д.б напечатано
	private String oldParTextNumber = "";//цифра прописью, которая была напечатана
	private String oldInputText = "";//Принятый ранее ответ
	private String Clarification = "";//поясняющая строка

	public String getOtvet() {
		return otvet;
	}

	public void setOtvet(String otvet) {
		this.otvet = otvet;
	}

	public String getParTextNumber() {
		return parTextNumber;
	}

	public void setParTextNumber(String parTextNumber) {
		this.parTextNumber = parTextNumber;
	}

	public String getOldParTextNumber() {
		return oldParTextNumber;
	}

	public void setOldParTextNumber(String oldParTextNumber) {
		this.oldParTextNumber = oldParTextNumber;
	}

	public String getClarification() {
		return Clarification;
	}

	public void setClarification(String clarification) {
		Clarification = clarification;
	}

	public String getOldInputText() {
		return oldInputText;
	}

	public void setOldInputText(String oldInputText) {
		this.oldInputText = oldInputText;
	}

}