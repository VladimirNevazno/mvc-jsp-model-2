package com.belhard.v1Class;

public class MemoryBean {

	private String otvet = "";
	private String checkTheAnswer = "";
	private String stroka1 = "";
	private String stroka2 = "";
	private String stroka3 = "";

	public String getOtvet() {
		return otvet;
	}
	
	public void setOtvet(String memory) {
		this.otvet = memory;
	}

	public String getCheckTheAnswer() {
		return checkTheAnswer;
	}

	public void setCheckTheAnswer(String checkTheAnswer) {
		this.checkTheAnswer = checkTheAnswer;
	}

	public String getStroka1() {
		return stroka1;
	}

	public void setStroka1(String stroka1) {
		this.stroka1 = stroka1;
	}

	public String getStroka2() {
		return stroka2;
	}

	public void setStroka2(String stroka2) {
		this.stroka2 = stroka2;
	}

	public String getStroka3() {
		return stroka3;
	}

	public void setStroka3(String stroka3) {
		this.stroka3 = stroka3;
	}
}
