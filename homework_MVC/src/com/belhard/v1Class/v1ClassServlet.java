package com.belhard.v1Class;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.belhard.v1Class.MemoryBean;
import com.belhard.v1Class.RequestUtils;

public class v1ClassServlet extends HttpServlet  {
	
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int parInt = 0;
		
		request.setCharacterEncoding("UTF-8");
		String parameter = RequestUtils.getParameter(request, "param");
		parInt=Integer.parseInt(parameter);
		MemoryBean memory1 = new MemoryBean();
		
		if (parInt==3) memory1.setOtvet("Правильно");
		if (parInt==1 || parInt==2 || parInt==4) memory1.setOtvet("Не правильно");
		if (parInt==0) memory1.setOtvet(" ");
		
		if (parInt!=0) {
		memory1.setStroka1("<br/><center>Вы выбрали</center>");
		memory1.setStroka2("<br/><center><img src='pictureTest/"+parInt+".jpg' height='140' ></center>");
		memory1.setStroka3("<br/><h1>"+memory1.getOtvet()+"</h1>");
		}
		
		//получили и снова запаковали сессию
		HttpSession session = request.getSession();
		session.setAttribute("memory1", memory1);
		
		//формирование ответа
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/pages/zadanie_1_MVC.jsp");
		dispatcher.forward(request, response);
	}
}
