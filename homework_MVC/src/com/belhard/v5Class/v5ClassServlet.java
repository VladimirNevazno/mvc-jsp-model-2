package com.belhard.v5Class;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class v5ClassServlet extends HttpServlet {
		
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		MemoryBean memory5 = new MemoryBean();
		
		Double doubInParamDlina = RequestUtils.getDoubleParameter(request, "paramDlina");
		Double doubInParamShirina = RequestUtils.getDoubleParameter(request, "paramShirina");
		if (doubInParamDlina==null) doubInParamDlina=0.0;
		if (doubInParamShirina==null) doubInParamShirina=0.0;
		
		memory5.setDlina(""+doubInParamDlina);
		memory5.setShirina(""+doubInParamShirina);
		memory5.setPerimetr(""+(2*doubInParamDlina+2*doubInParamShirina));
		memory5.setPloshad(""+doubInParamDlina*doubInParamShirina);
		if (doubInParamDlina==0 || doubInParamShirina==0)
			memory5.setDlinaDiagonali("введите корректные данные");
		else memory5.setDlinaDiagonali(""+Math.sqrt(doubInParamDlina*doubInParamDlina+doubInParamShirina*doubInParamShirina));
		
		
		//получили и снова запаковали сессию
				HttpSession session = request.getSession();
				session.setAttribute("memory5", memory5);
				
				//формирование ответа
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/pages/zadanie_5_MVC.jsp");
				dispatcher.forward(request, response);
	}

}
