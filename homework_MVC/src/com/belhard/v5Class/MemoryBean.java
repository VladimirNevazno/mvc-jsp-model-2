package com.belhard.v5Class;

public class MemoryBean {

	private String dlina = "";
	private String shirina = "";
	private String perimetr = "";
	private String ploshad = "";
	private String dlinaDiagonali = "";

	public String getDlina() {
		return dlina;
	}

	public void setDlina(String dlina) {
		this.dlina = dlina;
	}

	public String getShirina() {
		return shirina;
	}

	public void setShirina(String shirina) {
		this.shirina = shirina;
	}

	public String getPerimetr() {
		return perimetr;
	}

	public void setPerimetr(String perimetr) {
		this.perimetr = perimetr;
	}

	public String getPloshad() {
		return ploshad;
	}

	public void setPloshad(String ploshad) {
		this.ploshad = ploshad;
	}

	public String getDlinaDiagonali() {
		return dlinaDiagonali;
	}

	public void setDlinaDiagonali(String dlinaDiagonali) {
		this.dlinaDiagonali = dlinaDiagonali;
	}

}