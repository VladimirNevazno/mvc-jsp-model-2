package com.belhard.v10Class;

import java.util.ArrayList;

public class MemoryBean {
	private ArrayList<String> list = new ArrayList<String>();//список покупок
	private String basketOfGoods = "";

	public String getBasketOfGoods() {
		return basketOfGoods;
	}

	public void setBasketOfGoods(String basketOfGoods) {
		this.basketOfGoods = basketOfGoods;
	}

	public ArrayList<String> getList() {
		return list;
	}

	public void setList(ArrayList<String> list) {
		this.list = list;
	}
	//добавляем товар к списку
	public void addList(String str) {
		this.list.add(str);
	}

}