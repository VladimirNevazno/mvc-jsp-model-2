package com.belhard.v10Class;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class v10ClassServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		//response.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		// приём из сессии
		MemoryBean memory10 = (MemoryBean) session.getAttribute("memory10");
		if (memory10 == null) memory10 = new MemoryBean();

		// приём параметров
		String inputProductName = request.getParameter("productName");
		String inputDeleteProduct = request.getParameter("deleteProduct");
		
		//тут бизнес-код. добавляем товар в корзину и в строку покупки
		if (inputProductName!=null) {
			memory10.addList(inputProductName);
			String str = "";
			for (int i=0; i<memory10.getList().size();i++) {
				str =str +"<button type='submit' name='deleteProduct' value="+memory10.getList().get(i)+">удалить</button>"+memory10.getList().get(i)+"<br>";
			}
			memory10.setBasketOfGoods(str);
		}
		// "<a href='class_1_10.class?delete="+i+">удалить</a>" 
		//"<button type='submit' name='delete' value="+i+">удалить</button>"
		if (inputDeleteProduct!=null && memory10.getList().contains(inputDeleteProduct)) {			
			memory10.getList().remove(inputDeleteProduct);
			String str = "";
			for (int i=0; i<memory10.getList().size();i++) {
				str =str +"<button type='submit' name='deleteProduct' value="+memory10.getList().get(i)+">удалить</button>"+ memory10.getList().get(i)+"<br>";
			}
			memory10.setBasketOfGoods(str);
		}
//catch(IndexOutOfBoundsException e) {}
		// запаковали сессию
		session.setAttribute("memory10", memory10);

// формирование ответа
RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/pages/zadanie_10_MVC.jsp");
dispatcher.forward(request, response);
	}
}
