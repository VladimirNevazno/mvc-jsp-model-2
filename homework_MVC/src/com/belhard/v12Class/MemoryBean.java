package com.belhard.v12Class;

public class MemoryBean {
	private String myScript = "";
	private String chislo = "";

	public String getMyScript() {
		return myScript;
	}

	public void setMyScript(String myScript) {
		this.myScript = myScript;
	}

	public String getChislo() {
		return chislo;
	}

	public void setChislo(String chislo) {
		this.chislo = chislo;
	}
}