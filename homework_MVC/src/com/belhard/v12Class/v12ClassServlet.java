package com.belhard.v12Class;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class v12ClassServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		// response.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		// приём из сессии
		MemoryBean memory12 = (MemoryBean) session.getAttribute("memory12");
		if (memory12 == null)
			memory12 = new MemoryBean();

		// приём параметров
		String inputText = request.getParameter("paramText");

		// тут бизнес-код.
		if (inputText != null) {
			String[] tmp = inputText.split(" ");

			String str1 = "<script src=\"https://www.google.com/jsapi\"></script>"
					+ "<script>"
					+ "google.load(\"visualization\", \"1\", {packages:[\"corechart\"]});"
					+ "google.setOnLoadCallback(drawChart);"
					+ "function drawChart() {"
					+ "var data = google.visualization.arrayToDataTable(["
					+ "    ['Год'";
			String str2 = "";
			for (int i = 0; i < tmp.length; i++) {
				str2 = str2 + ", '" + tmp[i] + "'";
			}
			String str3 = "], ['цифра'";
			// ", '1', '2', '3', '4', '5', '6', '7', '8', '9', '10'"
			String str4 = "";
			for (int i = 0; i < tmp.length; i++) {
				str4 = str4 + ", " + tmp[i];
			}
			// , 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
			String str5 = "], ]);"

					+ "    var options = {"

					+ "    };"
					+ "    var chart = new google.visualization.ColumnChart(document.getElementById('oil'));"
					+ "    chart.draw(data, options);" + "   }" + "  </script>";
			memory12.setMyScript(str1 + str2 + str3 + str4 + str5);
		}

		// запаковали сессию
		session.setAttribute("memory12", memory12);

		// формирование ответа
		RequestDispatcher dispatcher = request
				.getRequestDispatcher("/WEB-INF/pages/zadanie_12_MVC.jsp");
		dispatcher.forward(request, response);
	}
}
