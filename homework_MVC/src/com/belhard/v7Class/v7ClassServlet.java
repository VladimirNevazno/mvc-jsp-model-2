package com.belhard.v7Class;

import java.io.IOException;
import java.util.Arrays;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class v7ClassServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		// приём из сессии
		MemoryBean memory7 = (MemoryBean) session.getAttribute("memory7");
		if (memory7 == null)
			memory7 = new MemoryBean();

		// приём параметров
		// 1
		String inputParam1 = request.getParameter("param1");
		if (inputParam1 == null)
			inputParam1 = "0";
		int inParam1 = Integer.parseInt(inputParam1);
		// 2
		String inputPicture = request.getParameter("paramPicture");

		// поступление списка картинок
		if (inputPicture != null) {
			// записываем массив наваний файлов, получаем длинну массива, индекс
			// делаем 0, отображаем первую картинку
			String[] tmp = inputPicture.split(":");
			memory7.setNamesOfictures(tmp);
			memory7.setLengthOfTheArray(Arrays.asList(tmp).size());
			memory7.setIndexPicture(0);
			tmp = memory7.getNamesOfictures();
			memory7.setPictureName(tmp[0]);
		}
		// Назад: индекс-1, картинка-1
		if (inParam1 == 1 && memory7.getNamesOfictures() != null
				&& memory7.getIndexPicture() > 0) {
			String[] tmp = memory7.getNamesOfictures();
			memory7.setIndexPicture(memory7.getIndexPicture() - 1);
			memory7.setPictureName(tmp[memory7.getIndexPicture()]);
		}
		// Вперёд: индекс+1, картинка+1
		if (inParam1 == 2 && memory7.getNamesOfictures() != null
				&& memory7.getIndexPicture() < memory7.getLengthOfTheArray()-1) {
			String[] tmp = memory7.getNamesOfictures();
			memory7.setIndexPicture(memory7.getIndexPicture() + 1);
			memory7.setPictureName(tmp[memory7.getIndexPicture()]);
		}

		// запаковали сессию
		session.setAttribute("memory7", memory7);

		// формирование ответа
		RequestDispatcher dispatcher = request
				.getRequestDispatcher("/WEB-INF/pages/zadanie_7_MVC.jsp");
		dispatcher.forward(request, response);
	}
}
