package com.belhard.v11Class;

import java.util.HashMap;
import java.util.Map;

public final class Questions {
	
	private final static String[] questions = {"Выберите название живого существа.",
		"Кто из них с иголками на спине?", "В какой цвет покрашена пожарная машина?"};
	
	private final static String[] answers1 = {"котейка", "молоток", "солнышко", "кулебяка"};
	private final static String[] answers2 = {"ёжик", "панда", "тюлень", "крабик"};
	private final static String[] answers3 = {"красный", "розовый", "белый", "голубой"};
	
	private static Map<String, String> answerMap = new HashMap<String, String>();
	static {
		answerMap.put("Выберите название живого существа.", "котейка");
		answerMap.put("Кто из них с иголками на спине?", "ёжик");
		answerMap.put("В какой цвет покрашена пожарная машина?", "красный");
	}
	//а щас буду делать метод который
	//выдаёт случайный вопрос
	static String getQuestion(int i) {
		if (i>=0&&i<=2)	return questions[i];
		else return null;
	}
	//выдаёт варианты ответов
	static String[] getAnswers(int i) {
		if (i==0) return answers1;
		if (i==1) return answers2;
		if (i==2) return answers3;
		else return null;
	}
	//сверяет принятый ответ с правильным
	static boolean checkTheAnswer(String str, int i) {
		if (answerMap.get(questions[i]).equals(str)) return true;
		else return false;
	}
}
