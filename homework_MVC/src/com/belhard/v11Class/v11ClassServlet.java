package com.belhard.v11Class;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class v11ClassServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		// приём из сессии
		MemoryBean memory11 = (MemoryBean) session.getAttribute("memory11");
		if (memory11 == null) memory11 = new MemoryBean();

		// приём параметров
		String inputOtvet = request.getParameter("otvet");
		
		//тут бизнес-код.
	if (inputOtvet==null) {
		//затираем ответную часть
		memory11.setOldVopros("");
		memory11.setOldOtvet("");
		memory11.setOtvet("");
		memory11.setButton("");
		//Формируем вопрос с ответами
		Random generator = new Random();
		int i = generator.nextInt(3); memory11.setIndex(i);
		//запись вопроса с сохранением предыдущего
		memory11.setVopros(Questions.getQuestion(i));
		//запись вариантов ответов
		ArrayList<String> list = new ArrayList<String>();
		list.add(Questions.getAnswers(i)[0]);
		list.add(Questions.getAnswers(i)[1]);
		list.add(Questions.getAnswers(i)[2]);
		list.add(Questions.getAnswers(i)[3]);
		Collections.shuffle(list);
		memory11.setVariant1("<button type='submit' name='otvet' value='"+list.get(0)+"'>"+list.get(0)+"</button><br>");
		memory11.setVariant2("<button type='submit' name='otvet' value='"+list.get(1)+"'>"+list.get(1)+"</button><br>");
		memory11.setVariant3("<button type='submit' name='otvet' value='"+list.get(2)+"'>"+list.get(2)+"</button><br>");
		memory11.setVariant4("<button type='submit' name='otvet' value='"+list.get(3)+"'>"+list.get(3)+"</button>");
	}
		//обработка ответа
		if (inputOtvet!=null) {
			memory11.setOldVopros("Был вопрос: "+memory11.getVopros());
			memory11.setVopros("");
			memory11.setVariant1(""); memory11.setVariant2("");memory11.setVariant3("");memory11.setVariant4("");
			memory11.setOldOtvet("Вы выбрали: "+inputOtvet);
			if (Questions.checkTheAnswer(inputOtvet, memory11.getIndex())) memory11.setOtvet("Правильно");
			else memory11.setOtvet("Не правильно");
			memory11.setButton("<a href='class_1_11.class'>далее</a>");
		}
			
		response.setCharacterEncoding("UTF-8");
		// запаковали сессию
		session.setAttribute("memory11", memory11);

// формирование ответа
RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/pages/zadanie_11_MVC.jsp");
dispatcher.forward(request, response);
System.out.println(response.getCharacterEncoding());
	}
}
