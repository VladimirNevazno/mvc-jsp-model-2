package com.belhard.v11Class;

public class MemoryBean {
	private String vopros = "";//Вопрос
	private String variant1 = "";//варианты ответа
	private String variant2 = "";//...
	private String variant3 = "";//...
	private String variant4 = "";//...
	private String oldVopros = "";//старый вопрос
	private String oldOtvet = "";//старый ответ
	private String otvet = "";//ответ
	private int index = 0;//индекс
	private String button = "";//Кнопка следующего задания
	
	public String getVopros() {
		return vopros;
	}
	public void setVopros(String vopros) {
		this.vopros = vopros;
	}
	public String getVariant1() {
		return variant1;
	}
	public void setVariant1(String variant1) {
		this.variant1 = variant1;
	}
	public String getVariant2() {
		return variant2;
	}
	public void setVariant2(String variant2) {
		this.variant2 = variant2;
	}
	public String getVariant3() {
		return variant3;
	}
	public void setVariant3(String variant3) {
		this.variant3 = variant3;
	}
	public String getVariant4() {
		return variant4;
	}
	public void setVariant4(String variant4) {
		this.variant4 = variant4;
	}
	public String getOldVopros() {
		return oldVopros;
	}
	public void setOldVopros(String oldVopros) {
		this.oldVopros = oldVopros;
	}
	public String getOldOtvet() {
		return oldOtvet;
	}
	public void setOldOtvet(String oldOtvet) {
		this.oldOtvet = oldOtvet;
	}
	public String getOtvet() {
		return otvet;
	}
	public void setOtvet(String otvet) {
		this.otvet = otvet;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getButton() {
		return button;
	}
	public void setButton(String button) {
		this.button = button;
	}
	
}