package com.belhard.v8Class;

public class TranslitBean {

	private String russianText;

	private String translitText;

	public TranslitBean(String russianText) {
		this.russianText = russianText;
		this.translitText = TranslitService.strToTranslit(russianText);
	}

	public String getRussianText() {
		return russianText;
	}

	public String getTranslitText() {
		return translitText;
	}
}
