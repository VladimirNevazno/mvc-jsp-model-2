package com.belhard.v8Class;

import java.util.HashMap;
import java.util.Map;

public final class TranslitService {

	public static final Map<Character, String> translitMap = new HashMap<Character, String>();

	static {
		translitMap.put('А', "A"); 
		translitMap.put('Б', "B");
		translitMap.put('В', "V");
		translitMap.put('Г', "G");
		translitMap.put('Д', "D");
		translitMap.put('Е', "E");
		translitMap.put('Ё', "Yo"); 
		translitMap.put('Ж', "Zh");
		translitMap.put('З', "Z");
		translitMap.put('И', "I");
		translitMap.put('Й', "J");
		translitMap.put('К', "K");
		translitMap.put('Л', "L"); 
		translitMap.put('М', "M");
		translitMap.put('Н', "N");
		translitMap.put('О', "O");
		translitMap.put('П', "P");
		translitMap.put('Р', "R");
		translitMap.put('С', "S"); 
		translitMap.put('Т', "T");
		translitMap.put('У', "U");
		translitMap.put('Ф', "F");
		translitMap.put('Х', "H");
		translitMap.put('Ц', "C"); 
		translitMap.put('Ч', "Ch");
		translitMap.put('Ш', "Sh");
		translitMap.put('Щ', "Shch");
		translitMap.put('Ы', "Y");
		translitMap.put('Ь', "'");
		translitMap.put('Ъ', "'");
		translitMap.put('Э', "E"); 
		translitMap.put('Ю', "U");
		translitMap.put('Я', "Ya");

		translitMap.put('а', "a"); 
		translitMap.put('б', "b");
		translitMap.put('в', "v");
		translitMap.put('г', "g");
		translitMap.put('д', "d");
		translitMap.put('е', "e");
		translitMap.put('ё', "yo"); 
		translitMap.put('ж', "zh");
		translitMap.put('з', "z");
		translitMap.put('и', "i");
		translitMap.put('й', "j");
		translitMap.put('к', "k");
		translitMap.put('л', "l"); 
		translitMap.put('м', "m");
		translitMap.put('н', "n");
		translitMap.put('о', "o");
		translitMap.put('п', "p");
		translitMap.put('р', "r");
		translitMap.put('с', "s"); 
		translitMap.put('т', "t");
		translitMap.put('у', "u");
		translitMap.put('ф', "f");
		translitMap.put('х', "h");
		translitMap.put('ц', "c"); 
		translitMap.put('ч', "ch");
		translitMap.put('ш', "sh");
		translitMap.put('щ', "shch");
		translitMap.put('ы', "y");
		translitMap.put('ъ', "'");
		translitMap.put('ь', "'");
		translitMap.put('э', "E"); 
		translitMap.put('ю', "u");
		translitMap.put('я', "ya");
	}

	private TranslitService() {
		super();
	}
//заменяет символы русских букв на английские(берёт строку и возвращает строку)
	public static String strToTranslit(String str) {
		char[] russianTextChars = str.toCharArray();
		StringBuilder translitTextStrings = new StringBuilder();

		for(int i = 0; i < russianTextChars.length; i++){
			char symbol = russianTextChars[i];
			String translitSymbol = TranslitService.symbolToTranslit(symbol);
			translitTextStrings.append(translitSymbol);			
		}

		return translitTextStrings.toString();
	}

	public static String symbolToTranslit(char symbol) {
		String translit = translitMap.get(symbol);
		if (translit != null) {
			return translit;
		}

		return Character.toString(symbol);
	}
}
