package com.belhard.v8Class;

import javax.servlet.http.HttpServletRequest;

public final class RequestUtils {

	private RequestUtils() {
		super();
	}

	public static String getParameter(HttpServletRequest request,
			String paramName) {
		if (StringUtils.isBlank(paramName)) {
			throw new IllegalArgumentException();
		}

		String value = request.getParameter(paramName);
		if (value != null) {
			return value;
		}

		return StringUtils.EMPTY_STR;
	}
}
