package com.belhard.v8Class;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class v8ClassServlet extends HttpServlet {

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String russianText = RequestUtils.getParameter(request, "enter");
		TranslitBean translitBean = new TranslitBean(russianText);

		HttpSession session = request.getSession();
		session.setAttribute("translitBean", translitBean);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/pages/zadanie_8_MVC.jsp");
		dispatcher.forward(request, response);
	}
}
